from guardian import GuardState
from isiguardianlib.sensor_correction.states import *
from isiguardianlib.sensor_correction.fader import Fader
from isiguardianlib.sensor_correction import edges as gen_edges
from sei_config.blend import const

prefix = 'ISI-ETMY_ST2'
nominal = 'CONFIG_250MHZ'
fade_time = 7


class INIT(GuardState):
    """Check the current configuration and then go to that state.
    If it is in an unknown state, we need to figure out where we
    should go.

    In the mean time maybe we should have this state just go to down?
    """

    def main(self):
        for config, dof_dict in const.BLEND_CONFIGS.items():
            if check_config_filter_engaged(dof_dict, bank='BLND'):
                return 'CONFIG_{}'.format(config)
        log('In unknown state, parking in DOWN')
        return 'DOWN'


# A parking spot for us to run tests, or to go after INIT when unknown
class DOWN(GuardState):
    index = 5

    def run(self):
        return True


make_config_states(const.BLEND_CONFIGS, bank='BLND', ramp=fade_time)
edges = gen_edges.make_gen_edges(const.BLEND_CONFIGS)
