from isiguardianlib import const as top_const

# Make sure that you are consistent with your dofs


BLEND_FILTER_SELECTION = {
    'ETMY' : {
        'BSC_ST1' : {
            '45MHZ': {
                'X': 8,
                'Y': 8,
                'Z': 8,
                'RX': 3,
                'RY': 3,
                'RZ': 3},
            'QUITE250': {
                'X': 3,
                'Y': 3,
                'Z': 8,
                'RX': 3,
                'RY': 3,
                'RZ': 3},
            'mH105': {
                'X': 7,
                'Y': 7,
                'Z': 8,
                'RX': 5,
                'RY': 3,
                'RZ': 3},
            'XYZ105': {
                'X': 7,
                'Y': 7,
                'Z': 7,
                'RX': 3,
                'RY': 3,
                'RZ': 3},
            'XYZ250': {
                'X': 3,
                'Y': 3,
                'Z': 4,
                'RX': 3,
                'RY': 3,
                'RZ': 3},
            'HIGH_BLEND': {
                'X': 1,
                'Y': 1,
                'Z': 1,
                'RX': 1,
                'RY': 1,
                'RZ': 1},
        },
        'BSC_ST2' : {
            'HIGH_BLEND': {
                'X': 2,
                'Y': 2,
                'Z': 2,
                'RX': 2,
                'RY': 2,
                'RZ': 2},
            'M102MHZ' : {
                'X' : 3,
                'Y' : 3,
                'Z' : 1,
                'RX' : 5,
                'RY' : 5,
                'RZ' : 1},
            '250MHZ': {
                'X': 1,
                'Y': 1,
                'Z': 1,
                'RX': 5,
                'RY': 5,
                'RZ': 1},
            'COMP250': {
                'X': 4,
                'Y': 4,
                'Z': 4,
                'RX': 5,
                'RY': 5,
                'RZ': 4},
        },
    },
    'ETMX' : {
        'BSC_ST1' : {
            '45MHZ': {
                'X': 8,
                'Y': 8,
                'Z': 8,
                'RX': 4,
                'RY': 4,
                'RZ': 3},
            'QUITE250': {
                'X': 4,
                'Y': 3,
                'Z': 8,
                'RX': 4,
                'RY': 4,
                'RZ': 3},
            'mH105': {
                'X': 7,
                'Y': 7,
                'Z': 8,
                'RX': 4,
                'RY': 5,
                'RZ': 3},
            'XYZ105': {
                'X': 7,
                'Y': 7,
                'Z': 7,
                'RX': 4,
                'RY': 4,
                'RZ': 3},
            'XYZ250': {
                'X': 4,
                'Y': 3,
                'Z': 4,
                'RX': 4,
                'RY': 4,
                'RZ': 3},
            'HIGH_BLEND': {
                'X': 1,
                'Y': 1,
                'Z': 1,
                'RX': 1,
                'RY': 1,
                'RZ': 1},
        },
        'BSC_ST2' : {
            'HIGH_BLEND': {
                'X': 2,
                'Y': 2,
                'Z': 2,
                'RX': 2,
                'RY': 2,
                'RZ': 2},
            'M102MHZ' : {
                'X' : 3,
                'Y' : 3,
                'Z' : 1,
                'RX' : 5,
                'RY' : 5,
                'RZ' : 1},
            '250MHZ': {
                'X': 1,
                'Y': 1,
                'Z': 1,
                'RX': 5,
                'RY': 5,
                'RZ': 1},
            'COMP250': {
                'X': 4,
                'Y': 4,
                'Z': 4,
                'RX': 5,
                'RY': 5,
                'RZ': 4},
        },
    },
    'ITMY' : {
        'BSC_ST1' : {
            '45MHZ' : {
                'X' : 8,
                'Y' : 8,
                'Z' : 8,
                'RX' : 4,
                'RY' : 4,
                'RZ' : 3},
            'QUITE250' : {
                'X' : 4,
                'Y' : 3,
                'Z' : 8,
                'RX' : 4,
                'RY' : 4,
                'RZ' : 3},
            'mH105' : {
                'X' : 7,
                'Y' : 7,
                'Z' : 8,
                'RX' : 5,
                'RY' : 4,
                'RZ' : 3},
            'XYZ105': {
                'X': 7,
                'Y': 7,
                'Z': 7,
                'RX': 4,
                'RY': 4,
                'RZ': 3},
            'XYZ250' : {
                'X' : 4,
                'Y' : 3,
                'Z' : 4,
                'RX' : 4,
                'RY' : 4,
                'RZ' : 3},
            'HIGH_BLEND': {
                'X': 1,
                'Y': 1,
                'Z': 1,
                'RX': 1,
                'RY': 1,
                'RZ': 1},
        },
        'BSC_ST2' : {
            'HIGH_BLEND': {
                'X': 2,
                'Y': 2,
                'Z': 2,
                'RX': 2,
                'RY': 2,
                'RZ': 2},
            'M102MHZ' : {
                'X' : 3,
                'Y' : 3,
                'Z' : 1,
                'RX' : 5,
                'RY' : 5,
                'RZ' : 1},
            '250MHZ': {
                'X': 1,
                'Y': 1,
                'Z': 1,
                'RX': 5,
                'RY': 5,
                'RZ': 1},
            'COMP250': {
                'X': 4,
                'Y': 4,
                'Z': 4,
                'RX': 5,
                'RY': 5,
                'RZ': 4},
        },
    },
    'ITMX' : {
        'BSC_ST1' : {
            '45MHZ': {
                'X': 8,
                'Y': 8,
                'Z': 8,
                'RX': 4,
                'RY': 4,
                'RZ': 4},
            'QUITE250': {
                'X': 4,
                'Y': 4,
                'Z': 8,
                'RX': 4,
                'RY': 4,
                'RZ': 4},
            'mH105': {
                'X': 7,
                'Y': 7,
                'Z': 8,
                'RX': 4,
                'RY': 5,
                'RZ': 4},
            'XYZ105': {
                'X': 7,
                'Y': 7,
                'Z': 7,
                'RX': 4,
                'RY': 4,
                'RZ': 3},
            'XYZ250': {
                'X': 4,
                'Y': 4,
                'Z': 4,
                'RX': 4,
                'RY': 4,
                'RZ': 4},
            'HIGH_BLEND': {
                'X': 1,
                'Y': 1,
                'Z': 1,
                'RX': 1,
                'RY': 1,
                'RZ': 1},
        },
        'BSC_ST2' : {
            'HIGH_BLEND': {
                'X': 2,
                'Y': 2,
                'Z': 2,
                'RX': 2,
                'RY': 2,
                'RZ': 2},
            'M102MHZ' : {
                'X' : 3,
                'Y' : 3,
                'Z' : 1,
                'RX' : 5,
                'RY' : 5,
                'RZ' : 1},
            '250MHZ': {
                'X': 1,
                'Y': 1,
                'Z': 1,
                'RX': 5,
                'RY': 5,
                'RZ': 1},
            'COMP250': {
                'X': 4,
                'Y': 4,
                'Z': 4,
                'RX': 5,
                'RY': 5,
                'RZ': 4},
        },
    },
    'BS' : {
        'BSC_ST1' : {
            '45MHZ': {
                'X': 8,
                'Y': 8,
                'Z': 8,
                'RX': 4,
                'RY': 4,
                'RZ': 3},
            'QUITE250': {
                'X': 3,
                'Y': 3,
                'Z': 8,
                'RX': 4,
                'RY': 4,
                'RZ': 3},
            'mH105': {
                'X': 7,
                'Y': 7,
                'Z': 8,
                'RX': 4,
                'RY': 4,
                'RZ': 3},
            'XYZ105': {
                'X': 7,
                'Y': 7,
                'Z': 7,
                'RX': 4,
                'RY': 4,
                'RZ': 3},                
            'XYZ250': {
                'X': 3,
                'Y': 3,
                'Z': 4,
                'RX': 4,
                'RY': 4,
                'RZ': 3},
            'HIGH_BLEND': {
                'X': 1,
                'Y': 1,
                'Z': 1,
                'RX': 1,
                'RY': 1,
                'RZ': 1},
        },
        'BSC_ST2' : {
            'HIGH_BLEND' : {
                'X' : 2,
                'Y' : 2,
                'Z' : 2,
                'RX' : 2,
                'RY' : 2,
                'RZ' : 2},
            'M102MHZ' : {
                'X' : 3,
                'Y' : 3,
                'Z' : 1,
                'RX' : 5,
                'RY' : 5,
                'RZ' : 1},
            '250MHZ' : {
                'X' : 1,
                'Y' : 1,
                'Z' : 1,
                'RX' : 5,
                'RY' : 5,
                'RZ' : 1},
            'COMP250': {
                'X': 4,
                'Y': 4,
                'Z': 4,
                'RX': 5,
                'RY': 5,
                'RZ': 4},},},
    'HAM2': {
        'HAM': {
            'HIGH_BLENDS': {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X': 1,
                'Y': 1,
                'Z': 1,
                'RX': 1,
                'RY': 1,
                'RZ': 1},
            'COMP250': {
                'X': 4,
                'Y': 4,
                'Z': 4,
                'RX': 4,
                'RY': 4,
                'RZ': 5}, 
            'M102': {
                'X': 7,
                'Y': 7,
                'Z': 7,
                'RX': 4,
                'RY': 4,
                'RZ': 5},}, },
    'HAM3': {
        'HAM': {
            'HIGH_BLENDS': {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X': 1,
                'Y': 1,
                'Z': 1,
                'RX': 1,
                'RY': 1,
                'RZ': 1},
            'COMP250': {
                'X': 4,
                'Y': 4,
                'Z': 4,
                'RX': 4,
                'RY': 4,
                'RZ': 5}, 
            'M102': {
                'X': 7,
                'Y': 7,
                'Z': 7,
                'RX': 4,
                'RY': 4,
                'RZ': 5},}, },
    'HAM4': {
        'HAM': {
            'HIGH_BLENDS': {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X': 1,
                'Y': 1,
                'Z': 1,
                'RX': 1,
                'RY': 1,
                'RZ': 1},
            'COMP250': {
                'X': 4,
                'Y': 4,
                'Z': 4,
                'RX': 4,
                'RY': 4,
                'RZ': 5}, 
            'M102': {
                'X': 7,
                'Y': 7,
                'Z': 7,
                'RX': 4,
                'RY': 4,
                'RZ': 5},}, },
    'HAM5': {
        'HAM': {
            'HIGH_BLENDS': {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X': 1,
                'Y': 1,
                'Z': 1,
                'RX': 1,
                'RY': 1,
                'RZ': 1},
            'COMP250': {
                'X': 4,
                'Y': 4,
                'Z': 4,
                'RX': 4,
                'RY': 4,
                'RZ': 5}, 
            'M102': {
                'X': 7,
                'Y': 7,
                'Z': 7,
                'RX': 4,
                'RY': 4,
                'RZ': 5},}, },
    'HAM6': {
        'HAM': {
            'HIGH_BLENDS': {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X': 1,
                'Y': 1,
                'Z': 1,
                'RX': 1,
                'RY': 1,
                'RZ': 1},
            'COMP250': {
                'X': 4,
                'Y': 4,
                'Z': 4,
                'RX': 4,
                'RY': 4,
                'RZ': 5}, 
            'M102': {
                'X': 7,
                'Y': 7,
                'Z': 7,
                'RX': 4,
                'RY': 4,
                'RZ': 5},}, },
    'HAM7': {
         'HAM': {
            'HIGH_BLENDS': {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X': 1,
                'Y': 1,
                'Z': 1,
                'RX': 1,
                'RY': 1,
                'RZ': 1},
            'COMP250': {
                'X': 4,
                'Y': 4,
                'Z': 4,
                'RX': 4,
                'RY': 4,
                'RZ': 5}, 
            'M102': {
                'X': 7,
                'Y': 7,
                'Z': 7,
                'RX': 4,
                'RY': 4,
                'RZ': 5},}, },
    'HAM8': {
        'HAM': {
            'HIGH_BLENDS': {
                # Keep dofs consistent, place None in if the
                # dof is not to be switched.
                'X' : 1,
                'Y' : 1,
                'Z' :1,
                'RX' : 1,
                'RY' : 1,
                'RZ' : 1},
            'COMP250': {
                'X' : 4,
                'Y' :4,
                'Z' : 4,
                'RX' :4,
                'RY' :4,
                'RZ' : 5},
            'COMP250Z45': {
                'X' : 4,
                'Y' : 4,
                'Z' : 3,
                'RX' : 4,
                'RY' : 4,
                'RZ' :5},
            'M102': {
                'X': 7,
                'Y': 7,
                'Z': 7,
                'RX': 4,
                'RY': 4,
                'RZ': 5},},},
}

BLEND_CONFIGS = BLEND_FILTER_SELECTION[top_const.CHAMBER][top_const.CHAMBER_TYPE]
